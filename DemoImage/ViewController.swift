//
//  ViewController.swift
//  DemoImage
//
//  Created by Truong Tran on 7/14/17.
//  Copyright © 2017 Truong Tran. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
  @IBOutlet weak var imageView: UIImageView!
  
  var image: UIImage? {
    didSet{
      show(image: image!)
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  @IBAction func onChoosePhoto(_ sender: UIButton) {
    pickPhoto()
  }

  @IBAction func onResize(_ sender: UIButton) {
    image = image?.resizedImage(withBounds: CGSize(width: 50, height: 50))
  }

  @IBAction func checkSize(_ sender: UIButton) {
    if let img = image {
      let imgData = NSData(data: UIImageJPEGRepresentation(img, 1)!)
      let imageSize = imgData.length
      print("***Size of image in kb: \(imageSize)")
      
    }
    
    
    
  }

  @IBAction func onUpload(_ sender: UIButton) {
    uploadImage()
  }
  func uploadImage() {
    guard let img = image else {
      print("*** Error image is nill")
      return
    }
    
    let imgData = NSData(data: UIImageJPEGRepresentation(img, 1)!)
    
    
    
    let randomNameImage = randomString(length: 5)
    let imageRef = FIRStorage.storage().reference().child(randomNameImage)
    let uploadTask = imageRef.put(imgData as Data, metadata: nil) {(metadata, error) in
      guard let metadata = metadata else {
        print("*** Error metadata is nill")
        return
      }
      print("success with link: \(metadata.downloadURL()!.absoluteString)")
      
    }
    
    // Listen for state changes, errors, and completion of the upload.
    uploadTask.observe(.resume) { snapshot in
      // Upload resumed, also fires when the upload starts
      print("Upload resumed, also fires when the upload starts")
    }
    
    uploadTask.observe(.pause) { snapshot in
      // Upload paused
      print("Upload paused")
    }
    
    uploadTask.observe(.progress) { snapshot in
      // Upload reported progress
      let percentComplete = 100.0 * Double(snapshot.progress!.completedUnitCount)
        / Double(snapshot.progress!.totalUnitCount)
      print("progress: \(percentComplete)%")
    }
    
    uploadTask.observe(.success) { snapshot in
      // Upload completed successfully
      print("Upload completed successfully")
      uploadTask.removeAllObservers()
    }
    
    uploadTask.observe(.failure) { snapshot in
      if let error = snapshot.error as NSError? {
        switch (FIRStorageErrorCode(rawValue: error.code)!) {
        case .objectNotFound:
          // File doesn't exist
          print("File doesn't exist")
          break
        case .unauthorized:
          // User doesn't have permission to access file
          print("User doesn't have permission to access file")
          break
        case .cancelled:
          // User canceled the upload
          print("User canceled the upload")
          break
          
        case .unknown:
          // Unknown error occurred, inspect the server response
          print("Unknown error occurred, inspect the server response")
          break
        default:
          // A separate error occurred. This is a good place to retry the upload.
          print("A separate error occurred. This is a good place to retry the upload.")
          break
        }
      }
    }

    
  }
  
  func randomString(length: Int) -> String {
    
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let len = UInt32(letters.length)
    
    var randomString = ""
    
    for _ in 0 ..< length {
      let rand = arc4random_uniform(len)
      var nextChar = letters.character(at: Int(rand))
      randomString += NSString(characters: &nextChar, length: 1) as String
    }
    
    return randomString
  }
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func takePhotoWithCamera() {
    let imagePicker = UIImagePickerController()
    imagePicker.view.tintColor = view.tintColor
    imagePicker.sourceType = .camera
    imagePicker.delegate = self
    imagePicker.allowsEditing = true
    present(imagePicker, animated: true, completion: nil)
  }
  func takePhotoFromLibrary() {
    let imagePicker = UIImagePickerController()
    imagePicker.view.tintColor = view.tintColor
    imagePicker.sourceType = .photoLibrary
    imagePicker.delegate = self
    imagePicker.allowsEditing = true
    present(imagePicker, animated: true, completion: nil)
  }
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
      self.image = image
    }

    dismiss(animated: true, completion: nil)
  }
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    dismiss(animated: true, completion: nil)
  }
  
  func pickPhoto() {
    if UIImagePickerController.isSourceTypeAvailable(.camera) {
      showPhotoMenu()
    } else {
      choosePhotoFromLibrary()
    }
  }
  func showPhotoMenu() {
    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    alertController.addAction(cancelAction)
    
    let takePhotoAction = UIAlertAction(title: "Take Photo", style: .default, handler: {_ in self.takePhotoWithCamera()})
    alertController.addAction(takePhotoAction)
    
    let chooseFromLibraryAction = UIAlertAction(title: "Choose From Library", style: .default, handler: {_ in self.takePhotoFromLibrary()})
    
    alertController.addAction(chooseFromLibraryAction)
    present(alertController, animated: true, completion: nil)
  }
  func choosePhotoFromLibrary() {
    
    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    alertController.addAction(cancelAction)
    
    let chooseFromLibraryAction = UIAlertAction(title: "Choose From Library", style: .default, handler: {_ in self.takePhotoFromLibrary()})
    
    alertController.addAction(chooseFromLibraryAction)
    present(alertController, animated: true, completion: nil)
    
  }
  func show(image: UIImage) {
    imageView.image = image
  }
}
